window.onload = () => {
  const shareButton = document.getElementById('share');
  const socials = document.getElementById('socials');
  shareButton.onclick = () => {
    if (shareButton.classList.length === 0) {
      shareButton.classList += 'share-active';
      socials.classList += 'active';
    }
    else {
      socials.classList = [];
      shareButton.classList = [];
    }
  }
};
